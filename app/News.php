<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public function news()
    {
        return $this->belongsTo(User::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

    public function category()
    {
        return $this->hasMany(Category::class);
    }
}
