<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $posts = News::where('title', 'LIKE', '%' . $request->q . '%')->orwhere('description', 'LIKE', '%' . $request->q . '%')->get();

        $search = $request->q;

        return view('search', compact('posts', 'search'));
    }
}
