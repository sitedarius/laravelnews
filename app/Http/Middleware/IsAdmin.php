<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin
{
    public function handle($request, Closure $next)
    {
        if (Auth::user()->permission == 'is_admin') {
            return $next($request);
        }

        return redirect()->route('login'); // If user is not an admin.
    }
}
