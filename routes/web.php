<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('HomeController@category');

Route::get('/', 'HomeController@index');

Route::get('/news', 'NewsController@index')->name('news');
Route::get('/news/{news}', 'NewsController@show')->name('show_news');
Route::get('/create', 'NewsController@create')->name('create_news');
Route::post('/news', 'NewsController@store')->name('store_news');
Route::get('/news/{news}/edit', 'NewsController@edit')->name('edit_news');
Route::patch('/news/{news}/update', 'NewsController@update')->name('update_news');
Route::delete('/news/{news}', 'NewsController@destroy')->name('delete_news');


Route::post('/search', 'SearchController@search')->name('search');
