@extends('layout.app')

@section('content')

    <div class="row">

        <div class="col-md-12">
            @if (count($posts))
                <h3>Results for: {{ $search }}</h3>
            @else
                <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10 justify-content-center text-center">
                    <img src="/storage/search/oops.png" class="w-100">
                    <h3 class="mt-5 pt-5">There are no results for: {{ $search }}</h3>
                </div>
                <div class="col-md-1"></div>
                </div>
            @endif
            <ul class="list-unstyled">
                <div class="row">
                    @foreach($posts as $post)
                        <div class="col-md-6 mt-2">
                            <div class="card" style="border-bottom: 5px solid #f6993f">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $post->title }}</h5>
                                    <p class="card-text">{{ $post->description }} <a href="/nieuws/{{ $post->id }}"
                                                                                     class="">Lees meer...</a></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </ul>


        </div>

    </div>

@endsection