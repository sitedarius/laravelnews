<!DOCTYPE html>
<html lang="NL">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Stylesheet -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="https://kit.fontawesome.com/6f85a7f239.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="{{ URL::asset('css/app_new.css') }}">
</head>
<body style="background: #f8f9fa;">

{{--Navigation--}}
<nav class="navbar navbar-expand-lg navbar-dark"
     style="border-bottom: 5px solid #ed7354; background-color: #626262!important;">
    <div class="container">
        <a class="navbar-brand" href="/">Het Noordelijk Nieuws</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <form class="form-inline my-2 my-lg-0 mx-auto" method="POST" action="{{ route('search') }}">
                @csrf
                <div class="p-1 bg-light rounded rounded-pill shadow-sm">
                    <div class="input-group">
                        <input type="search" placeholder="Zoeken..." aria-describedby="button-addon1"
                               class="form-control border-0 bg-light" name="q" id="q"
                               style="border-radius: 25px 0 0 25px;">
                        <div class="input-group-append">
                            <button id="button-addon1" type="submit" class="btn btn-link" style="color: #ed7354"><i
                                        class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </form>

            <ul class="navbar-nav ml-auto">
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Profile (N.A)</a>
                            @if(Auth::check() && Auth::user()->is_admin == true)
                                <a class="dropdown-item" href="#">Admin-page (N.A)</a>
                            @endif
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item text-danger" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Uitloggen') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest

            </ul>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand-lg navbar-light bg-white">
    <div class="container">
        <ul class="navbar-nav mx-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('news') }}">Nieuws</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container my-5">
    <div class="row">
        <div class="col-md-8">
            @yield('content')
        </div>
        <div class="col-md-4 mt-5">
            <div class="card">
                <div class="card-body">
                    <p class="mb-0">My HNN Account</p>
                    <hr>
                    @guest
                        <div class="row mt-2">
                            <div class="col-md-6 text-center">
                                <a type="button" href="{{ route('login') }}" class="btn btn-primary">Login</a>
                            </div>
                            <div class="col-md-6 text-center">
                                <a type="button" href="{{ route('register') }}" class="btn btn-primary">Register</a>
                            </div>
                            @else
                                <div class="col-md-8 pl-0">
                                    <p class="mb-0">Welcome {{ Auth::user()->name }},</p>
                                </div>
                                <div class="col-md-2 pl-0">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt"></i>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                        @csrf
                                    </form>
                                </div>
                        </div>
                    @endguest
                </div>
            </div>
        </div>
    </div>

</div>

</body>

{{--Scripts--}}
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<script>
    $('.delete-user').click(function (e) {
        e.preventDefault() // Don't post the form, unless confirmed
        if (confirm('Weet je het zeker?')) {
            // Post the form
            $(e.target).closest('form').submit() // Post the surrounding form
        }
    });
</script>

<script type="text/javascript">
    var route = "{{ url('autocomplete') }}";
    $('#search').typeahead({
        source: function (term, process) {
            return $.get(route, {term: term}, function (data) {
                return process(data);
            });
        }
    });
</script>


</html>
