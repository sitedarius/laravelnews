@extends('layout.app')

@section('title', 'Nieuws - Het Noordelijk Nieuws')

@section('content')

    <div class="row">
        <div class="col-md-6">
            <h3>News</h3>
        </div>

        <div class="col-md-6 pl-13">
            @if(Auth::check() && Auth::user()->is_admin == true)
                <a href="{{ route('create_news') }}" class="btn btn-success">Create news item</a>
            @endif
        </div>

        <div class="col-md-12">
            <ul class="list-unstyled">
                <div class="row">
                    @foreach($posts as $post)
                        <div class="col-md-6 mt-2">
                            <div class="card" style="border-bottom: 5px solid #ed7354">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $post->title }}</h5>
                                    <p class="card-text">{{ $post->description }}  <a href="{{ route('show_news', [$post->id]) }}" class="">Read more...</a> </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </ul>
        </div>
    </div>

@endsection