@extends('layout.app')

@section('title', 'Nieuws - Het Noordelijk Nieuws')

@section('content')

    <div class="row">
        <div class="col-md-8">
            <a href="{{ route('news') }}" class="" style="font-size: 20px; color: #f6993f;"><i
                    class="fas fa-arrow-left"></i></a>
        </div>
        @if(Auth::check() && Auth::user()->is_admin == true)
            <div class="col-md-9">
                <h2>{{ $post->title }}</h2>
                <p class="mb-0">{{ Carbon\Carbon::parse($post->created_at)->format('j F Y h:m') }}<br>
                    Laatste update: {{ Carbon\Carbon::parse($post->updated_at)->format('j F Y h:m') }}</p>
            </div>
            <div class="col-md-3">

                <div class="row">
                    <div class="col-md-6">
                        <a href="{{ route('edit_news', [$post->id]) }}" class="btn btn-success">Edit</a>
                    </div>
                    <div class="col-md-6">
                        <form method="POST" action="{{ route('delete_news', [$post->id]) }}">
                            @csrf
                            @method('DELETE')

                            <div class="form-group">
                                <input type="submit" class="btn btn-danger delete-user" value="Delete">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif

        <div class="col-md-12">
            <hr>
            <p>{!! $post->body !!}</p>
            <hr>
            <small>Author: {{ $post->user->name }}<br>By: {{ $post->user->job }}</small>
            <hr>
            <h3>Reacties</h3>
            <hr>
            @foreach($comments as $comment)
                <div class="card mt-2" style="border-radius: 0;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-1">
                                <img
                                    src="https://3.bp.blogspot.com/-yD9yoQeRW64/Wi988bKrkbI/AAAAAAAAC4w/SnZhLqI2hTsdubAfSoQaTIlcEKVUVzEMwCLcBGAs/s1600/20638096_1568526093204152_4116218239013763144_n.jpg"
                                class="w-100 mx-auto">
                            </div>
                            <div class="col-9">
                                <p class="mb-0"><strong>{{ $comment->user->name }}</strong></p>
                                <small>{{ Carbon\Carbon::parse($comment->created_at)->format('d-m-Y') }}</small>
                            </div>
                            <div class="col-2 text-right">
                                <p><button type="button" class="btn"><i class="far fa-heart"></i></button> <button type="button" class="btn"><i class="fas fa-share"></i></button></p>
                            </div>
                            <div class="col-12">
                                <hr>
                                <p>{{ $comment->comment }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>

@endsection
