@extends('layout.app')

@section('title', 'Nieuws - Het Noordelijk Nieuws')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('news') }}" class="" style="font-size: 20px; color: #f6993f;"><i
                        class="fas fa-arrow-left"></i></a>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h3>Create a news item:</h3>
                    <hr>
                    <form method="POST" action="{{ route('store_news') }}">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">Title*:</label>
                            <input type="text"
                                   class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                                   id="exampleInputEmail1" aria-describedby="emailHelp"
                                   name="title" value="{{ old('title') }}" required>
                            <p style="color: red;">{{ $errors->first('title') }}</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Intro*:</label>
                            <input type="text"
                                   class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
                                   id="exampleInputEmail1" aria-describedby="emailHelp"
                                   name="description" value="{{ old('description') }}" required>
                            <p style="color: red;">{{ $errors->first('description') }}</p>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Description*:</label>
                            <textarea class="description {{ $errors->has('body') ? 'is-invalid' : '' }}"
                                      name="body"></textarea>
                            <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
                            <script>
                                tinymce.init({
                                    selector: 'textarea.description',
                                    height: 500
                                });
                            </script>
                            <p style="color: red;">{{ $errors->first('body') }}</p>
                        </div>
                        <small>* Required</small>
                        <button type="submit" class="btn btn-primary mt-2 ml-auto">Save</button>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection